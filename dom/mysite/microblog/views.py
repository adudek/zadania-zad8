#!/usr/bin/python
# -*- coding: utf-8 -*-

from microblog.models import Post, Tag, LoginForm, PostForm, RegistrationForm, UserActivation
from django.shortcuts import render_to_response, redirect
from django.http import Http404
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
import random, string
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone


def user_login(request):
    if request.user.is_authenticated():
        return index(request)

    if request.method == "GET":
        form = LoginForm()

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
            if user:
                if user.is_active:
                    login(request, user)
                    return index(request, 'Udało się zalogować.')
                else:
                    return index(request, 'Użytkownik jest nieaktywny, nie może się logować.')
            else:
                message = 'Niewłaściwy login i hasło.'
    return render_to_response('blog/login.html', locals(), RequestContext(request))

def user_logout(request):
    if request.user.is_authenticated():
        logout(request)
        return index(request, 'Wylogowano!')
    return index(request)

def index(request, message=''):
    posts_list = Post.objects.order_by('-pub_date')
    return render_to_response('blog/index.html', locals(), RequestContext(request))

def tags_post(request, id):
    tag = Tag.objects.filter(pk=id)
    if not tag:
        raise Http404()
    tag = tag[0]
    posts = tag.articles.all()
    return render_to_response("blog/tag.html", locals(), RequestContext(request))

def edit(request, id):
    if not request.user.is_authenticated():
        return index(request,'Tylko dla zalogowanych')

    post = Post.objects.all().filter(pk=id)
    if not post:
        raise Http404()

    post = post[0]

    editable = False
    if post.mod_date:
        editable = (timezone.now() - post.mod_date).seconds < 600
    else:
        editable = (timezone.now() - post.pub_date).seconds < 600

    if request.user.groups.filter(name='moderator') or editable and post.pub_user_id == request.user.id:
        if request.method == "POST":
            form = PostForm(request.POST)
            if form.is_valid():
                post.title = form.cleaned_data['title']
                post.mod_date = timezone.now()
                post.mod_user = request.user
                post.text = form.cleaned_data['text']
                post.tag_set.clear()
                for tag in form.cleaned_data['tags']:
                    post.tag_set.add(tag)
                post.save()
                return redirect(reverse('index'))
        else:
            form = PostForm(initial={
               'title':post.title,
               'text':post.text,
               'tags': post.tag_set.all
            })
        return render_to_response("blog/edit.html", locals(), RequestContext(request))
    else:
        return index(request, 'Nie możesz edytować.')

def new(request):
    if not request.user.is_authenticated():
        return index(request,'Tylko dla zalogowanych')

    if request.method == "GET":
        form = PostForm()

    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = Post(title=form.cleaned_data['title'],
                        text=form.cleaned_data['text'],
                        pub_user = request.user,
                        pub_date = timezone.now(),
            )
            post.save()
            for tag in form.cleaned_data['tags']:
                post.tag_set.add(tag)
            post.save()
            return redirect(reverse('index'), 'Dodano wpis!')

    return render_to_response('blog/new.html', locals(), RequestContext(request))

def user_register(request):
    if request.user.is_authenticated():
        return index(request,'Brak dostepu.')

    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User()
            user.username = form.cleaned_data['username']
            user.set_password(form.cleaned_data['password1'])
            user.email = form.cleaned_data['email']
            user.is_active = False
            user.save()
            activation = UserActivation()
            activation.activation_key = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(6))
            activation.user = user
            activation.save()
            send_mail('Email aktywacyjny', 'http://194.29.175.240/~p3/users/activate/%s' % activation.activation_key, 'test@test.com', [user.email])
            return index(request,'Wysłano email aktywacyjny')
    else:
        form = RegistrationForm()
    return render_to_response("blog/register.html", locals(), RequestContext(request))

def activate(request, activation_key):
    activ = UserActivation.objects.all().filter(id=activation_key)
    if activ:
        to_user = User.objects.all().filter(pk=activ.user.id)
        to_user.is_active = True
        to_user.save()
        activ.delete()
        return index(request,'Możesz sie zalogować.')
    raise Http404()

def user_posts(request, id):
    pub_user = User.objects.all().filter(id=id)
    if pub_user:
        pub_user = pub_user[0]
        posts = Post.objects.all().filter(pub_user_id=id).order_by('-pub_date')
        return render_to_response('blog/user.html', locals(), RequestContext(request))
    raise Http404
