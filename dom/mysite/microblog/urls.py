from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name="index"),
    url(r'^edit/(?P<id>\d+)$', views.edit, name="edycja_posta"),
    url(r'^users/(?P<id>\d+)$', views.user_posts, name="posty_uzytkownika"),
    url(r'^tags/(?P<id>\d+)$', views.tags_post, name="posty_tagu"),
    url(r'^users/new$', views.user_register, name="nowy_uzytkownik"),
    url(r'^users/activate/(?P<activation_key>.+)$', views.activate, name="aktywacja_uzytkownika"),
    url(r'^authenticate$', views.user_login, name="nowa_sesja"),
    url(r'^logoff$', views.user_logout, name="zakoncz_sesje"),
    url(r'^add$', views.new, name="new")
)